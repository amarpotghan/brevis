mod context;
mod primitives;

use std::io;
use std::io::Write;
use std::num;
use context::*;

fn main() {
    let mut context = primitives::new_context();
    start(&mut context);
}

fn start(context: &mut Context) {
    loop {
        let mut next = String::new();
        print!(">>> ");

        io::stdout().flush().unwrap();

        let result = io::stdin()
            .read_line(&mut next)
            .map_err(|err| Error::Unexpected(err.to_string()))
            .map(|_x| next.split_whitespace())
            .and_then(|iter| outer_interpreter(context, iter));

        match result {
            Ok(_) => {
                println!("OK.");
            }
            Err(Error::Terminated) => {
                println!("Bye.");
                break;
            }
            Err(other) => {
                context.clear();
                println!("Interpreter: {}", other);
            }
        }
    }
}

fn interpret<'a, T>(context: &mut Context, original: &str, next: &mut T) -> Result<Jump, Error>
where
    T: Iterator<Item = &'a str>,
{
    let word = &original;
    let found = context.find(word);
    match context.current_mode() {
        Mode::Interpret => match found {
            None => parse(word, next)
                .map(|v| context.push(v))
                .and_then(|_| context::ok(1)),

            Some(w) => {
                let code = w.code.clone();
                code(context, next)
            }
        },

        Mode::Compile => match found {
            None => parse(word, next)
                .map(|v: Value| context.add_to_latest(lit_word(v)))
                .and_then(|_| context::ok(1)),

            Some(w) => {
                if w.immediate {
                    let code = w.code.clone();
                    code(context, next)
                } else {
                    let w = w.clone();
                    context.add_to_latest(w);
                    context::ok(1)
                }
            }
        },
    }
}

fn outer_interpreter<'a, T>(context: &mut Context, next: T) -> Result<Jump, Error>
where
    T: Iterator<Item = &'a str> + std::clone::Clone,
{
    let mut line = next;
    let mut re = 0;
    while let Some(word) = line.next() {
        re = interpret(context, word, &mut line).map(|addr| addr.absolute())?;
    }
    context::ok(re)
}

// Does not support quotes in the strings. Does support spaces in the strings.
fn parse<'a, T>(word: &str, next: &mut T) -> Result<Value, Error>
where
    T: Iterator<Item = &'a str>,
{
    if word.starts_with("\"") {
        if word.ends_with("\"") {
            Ok(Value::STRING(word.replace("\"", "")))
        } else {
            let from_next = {
                let mut result = "".to_owned();
                for s in next {
                    let s = format!(" {}", s);
                    if !s.ends_with("\"") && s != "\"" {
                        result.push_str(&s);
                    } else {
                        // first time
                        result.push_str(&s);
                        break;
                    }
                }
                result
            };

            let mut word = word.to_owned();
            if from_next.is_empty() {
                return Err(Error::Unexpected(format!("Bad string literal {}", word)));
            } else {
                let to_push = format!("{}", from_next);
                word.push_str(&to_push);
                // string literal
                return Ok(Value::STRING(word.replace("\"", "")))
            };
        }
    } else {
        // int
        word.parse()
            .map_err(|err: num::ParseIntError| Error::unexpected_word(word, &err.to_string()))
            .map(|i| Value::INT(i))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use Value::*;

    use std::rc;

    #[test]
    fn parsing() {
        let vec: Vec<&str> = vec!["x", "\"", "y", "z", "\"", "8"];
        let x = vec.iter();
        let r: Vec<&str> = x.take_while(|x| **x != "\"").cloned().collect();
        assert_eq!(r, vec!["x"]);

        let mut line = "\"XX\"".split_whitespace();
        if let Ok(r) = parse(line.next().unwrap(), &mut line) {
            assert_eq!(r, Value::STRING("XX".to_string()));
        }

        let mut line = "123XX".split_whitespace();

        if let Ok(r) = parse(line.next().unwrap(), &mut line) {
            assert_eq!(r, Value::STRING("123XX".to_string()));
        }

        let mut line = "123 \"LOL\"".split_whitespace();

        if let Ok(r) = parse(line.next().unwrap(), &mut line) {
            assert_eq!(r, Value::INT(123));
        }

        let mut line = "  \"LOL\"".split_whitespace();

        if let Ok(r) = parse(line.next().unwrap(), &mut line) {
            assert_eq!(r, Value::STRING("LOL".to_string()));
        }

        let mut line = "\"Hey There\"".split_whitespace();

        if let Ok(r) = parse(line.next().unwrap(), &mut line) {
            assert_eq!(r, Value::STRING("Hey There".to_string()));
        }

        let mut line = "\"Hey There lol hey\" \"LOLOLOL\"".split_whitespace();

        if let Ok(r) = parse(line.next().unwrap(), &mut line) {
            assert_eq!(r, Value::STRING("Hey There lol hey".to_string()));
        }


        let mut line = "\"Hey There lol hey \" \"LOLOLOL\" 1 2 3".split_whitespace();

        if let Ok(r) = parse(line.next().unwrap(), &mut line) {
            assert_eq!(r, Value::STRING("Hey There lol hey ".to_string()));
        }
    }

    #[test]
    fn if_then_else() {
        let expression = ": cond5 ( n -- n ) 5 < if 5 else 10 then ;".split_whitespace();
        let expression2 = "6 cond5".split_whitespace();
        let expression3 = "3 cond5".split_whitespace();
        let expression4 = "5 cond5".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        let _ = outer_interpreter(&mut context, expression2);
        assert_eq!(context.pop(), Ok(Value::INT(10)));
        let _ = outer_interpreter(&mut context, expression3);
        assert_eq!(context.pop(), Ok(Value::INT(5)));
        let _ = outer_interpreter(&mut context, expression4);
        assert_eq!(context.pop(), Ok(Value::INT(10)));
    }

    #[test]
    fn string_literal() {
        let expression = "\"hey\" 10 \"lol\" ".split_whitespace();
        let expression2 = ": WWW \"www\" ;".split_whitespace();
        let expression3 = "WWW".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        assert_eq!(context.pop(), Ok(Value::STRING("lol".to_string())));
        assert_eq!(context.pop(), Ok(Value::INT(10)));
        assert_eq!(context.pop(), Ok(Value::STRING("hey".to_string())));
        let _ = outer_interpreter(&mut context, expression2);
        let _ = outer_interpreter(&mut context, expression3);
        assert_eq!(context.pop(), Ok(Value::STRING("www".to_string())));
    }

    #[test]
    fn nested_if_then_else() {
        let expression =
            ": cond5 dup 5 < if 5 else 10 < if 10 else 100 then then ;".split_whitespace();
        let expression2 = "6 cond5".split_whitespace();
        let expression3 = "12 cond5".split_whitespace();
        let expression4 = "2 cond5".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        let _ = outer_interpreter(&mut context, expression2);
        assert_eq!(context.pop(), Ok(Value::INT(10)));
        let _ = outer_interpreter(&mut context, expression3);
        assert_eq!(context.pop(), Ok(Value::INT(100)));
        let _ = outer_interpreter(&mut context, expression4);
        assert_eq!(context.pop(), Ok(Value::INT(5)));
    }

    #[test]
    fn nested_if_then_else_2() {
        let expression =
            ": cond5 dup 5 < if 5 else dup 10 < if 7 < if 7 else 10 then else 100 then then ;"
                .split_whitespace();
        let expression2 = "6 cond5".split_whitespace();
        let expression3 = "12 cond5".split_whitespace();
        let expression4 = "2 cond5".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        let _ = outer_interpreter(&mut context, expression2);
        assert_eq!(context.pop(), Ok(Value::INT(7)));
        let _ = outer_interpreter(&mut context, expression3);
        assert_eq!(context.pop(), Ok(Value::INT(100)));
        let _ = outer_interpreter(&mut context, expression4);
        assert_eq!(context.pop(), Ok(Value::INT(5)));
    }

    #[test]
    fn nested_if_then_else_3() {
        let expression = ": cond5 5 < if 5 dup else 10 dup then  ;".split_whitespace();
        let expression2 = "4 cond5".split_whitespace();
        let expression3 = "6 cond5".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        let _ = outer_interpreter(&mut context, expression2);
        assert_eq!(context.pop(), Ok(Value::INT(5)));
        assert_eq!(context.pop(), Ok(Value::INT(5)));
        let _ = outer_interpreter(&mut context, expression3);
        assert_eq!(context.pop(), Ok(Value::INT(10)));
        assert_eq!(context.pop(), Ok(Value::INT(10)));
    }

    #[test]
    fn nested_if_then() {
        let expression = ": cond5 5 < if 5 dup 6 then  ;".split_whitespace();
        let expression2 = "4 cond5".split_whitespace();
        let expression3 = "6 cond5".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        let _ = outer_interpreter(&mut context, expression2);
        assert_eq!(context.pop(), Ok(Value::INT(6)));
        assert_eq!(context.pop(), Ok(Value::INT(5)));
        assert_eq!(context.pop(), Ok(Value::INT(5)));
        let _ = outer_interpreter(&mut context, expression3);
        assert_eq!(context.peek().is_err(), true);
    }

    #[test]
    fn loopy() {
        let expression = ": loopy 5 0 do i loop  ;".split_whitespace();
        let expression2 = "loopy".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        let _ = outer_interpreter(&mut context, expression2);
        assert_eq!(context.show_stack(), "0 1 2 3 4");
    }

    #[test]
    fn vatiable() {
        let expression = "variable a".split_whitespace();
        let expression2 = ": set 42 a ! ;".split_whitespace();
        let expression3 = ": get a @ ;".split_whitespace();
        let expression4 = "set get".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        let _ = outer_interpreter(&mut context, expression2);
        assert_eq!(context.show_stack(), "");
        let _ = outer_interpreter(&mut context, expression3);
        let _ = outer_interpreter(&mut context, expression4);
        assert_eq!(context.show_stack(), "42");
    }

    #[test]
    fn array_vatiable() {
        let expression = "10 array a".split_whitespace();
        let expression2 = ": set 42 9 a ! ;".split_whitespace();
        let expression3 = ": get 9 a @ ;".split_whitespace();
        let expression4 = "set get".split_whitespace();
        let expression5 = "10 a @ ;".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        let _ = outer_interpreter(&mut context, expression2);
        assert_eq!(context.show_stack(), "");
        let _ = outer_interpreter(&mut context, expression3);
        let _ = outer_interpreter(&mut context, expression4);
        assert_eq!(context.show_stack(), "42");
        if let Err(_c) = outer_interpreter(&mut context, expression5) {};
    }

    #[test]
    fn loop_plus() {
        let expression = ": loopy 20 0 do i 5 +loop  ;".split_whitespace();

        let expression2 = "loopy".split_whitespace();

        let mut context = primitives::new_context();
        let _ = outer_interpreter(&mut context, expression);
        let _ = outer_interpreter(&mut context, expression2);
        assert_eq!(context.show_stack(), "0 5 10 15");
        context.clear();

        let def2 = ": inc-by do i swap dup +loop drop ;".split_whitespace();
        let _ = outer_interpreter(&mut context, def2);
        let inc_by_1 = "1 5 0 inc-by";
        let _ = outer_interpreter(&mut context, inc_by_1.split_whitespace());

        assert_eq!(context.show_stack(), "0 1 2 3 4");

        context.clear();
        let inc_by_2 = "2 5 0 inc-by";
        let _ = outer_interpreter(&mut context, inc_by_2.split_whitespace());

        assert_eq!(context.show_stack(), "0 2 4");

        context.clear();

        let inc_by_2 = "-2 0 10 inc-by";
        let _ = outer_interpreter(&mut context, inc_by_2.split_whitespace());

        assert_eq!(context.show_stack(), "10 8 6 4 2");
    }

    #[test]
    fn test_immediate() {
        let mut context = primitives::new_context();
        let immediate = ": xx 10 ; immediate";
        let use_it = ": yy xx ;";
        let _ = outer_interpreter(&mut context, immediate.split_whitespace());
        let _ = outer_interpreter(&mut context, use_it.split_whitespace());
        assert_eq!(context.show_stack(), "10");
    }

    #[test]
    fn test_numeric() {
        let mut context = primitives::new_context();
        let expr = "1 1+ 2+ 2* 1- 2-";
        let _ = outer_interpreter(&mut context, expr.split_whitespace());
        assert_eq!(context.show_stack(), "5");
        let expr = "1 2 mod";
        let _ = outer_interpreter(&mut context, expr.split_whitespace());
        assert_eq!(context.show_stack(), "5 0");
        let expr = "2 2 3 * *";
        let _ = outer_interpreter(&mut context, expr.split_whitespace());
        assert_eq!(context.show_stack(), "5 0 12");
        let expr = "2 3 - 5 +";
        let _ = outer_interpreter(&mut context, expr.split_whitespace());
        assert_eq!(context.show_stack(), "5 0 12 4");
        let expr = "2 3 min neg";
        let _ = outer_interpreter(&mut context, expr.split_whitespace());
        assert_eq!(context.show_stack(), "5 0 12 4 -2");
        let expr = "0 1 xor 0 0 xor and 1 1 and";
        let _ = outer_interpreter(&mut context, expr.split_whitespace());
        assert_eq!(context.show_stack(), "5 0 12 4 -2 0 1");
        let expr = "+ + + * + +";
        let _ = outer_interpreter(&mut context, expr.split_whitespace());
        assert_eq!(context.show_stack(), "41");
    }

    #[test]
    fn test_begin_end() {
        let mut context = primitives::new_context();
        let immediate = ": 10times 0 begin dup 1+ dup 10 =  until ;";
        let use_it = "10times";
        let _ = outer_interpreter(&mut context, immediate.split_whitespace());
        let _ = outer_interpreter(&mut context, use_it.split_whitespace());
        assert_eq!(context.show_stack(), "0 1 2 3 4 5 6 7 8 9 10");
    }

    #[test]
    fn test_begin_end_2() {
        let mut context = primitives::new_context();
        let immediate = ": till begin >r dup 1- dup r> dup rot = until drop ;";
        let _ = outer_interpreter(&mut context, immediate.split_whitespace());
        let use_it = "20 10 till";
        let _ = outer_interpreter(&mut context, use_it.split_whitespace());
        assert_eq!(
            context.show_stack(),
            (10..21)
                .rev()
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
                .join(" ")
        );
    }

    #[test]
    fn outer_t() {
        let mut interpreter = Context::empty();

        let iter = vec!["123", "456"].into_iter();
        if let Ok(_x) = outer_interpreter(&mut interpreter, iter) {
            assert_eq!(interpreter.data_stack_len(), 2);
            assert_eq!(interpreter.pop().unwrap(), INT(456));
            assert_eq!(interpreter.pop().unwrap(), INT(123));
        }
    }

    #[test]
    fn int_lit_interpret() {
        let mut interpreter = Context::empty();

        let x: Vec<&str> = vec![];
        if let Ok(_x) = interpret(&mut interpreter, "123", &mut x.into_iter()) {
            assert_eq!(interpreter.data_stack_len(), 1);
            assert_eq!(interpreter.pop().unwrap(), INT(123))
        }
    }

    #[test]
    fn immediate_compile() {
        let code: NativeCode = rc::Rc::new(|x, _| {
            if let (INT(x1), INT(x2)) = (x.pop()?, x.pop()?) {
                x.push(INT(x1 + x2));
                context::ok(1)
            } else {
                Err(Error::Terminated)
            }
        });
        let immediate = true;
        let primitive = false;
        let name = "eager_plus";
        let word = Word::from(code, immediate, primitive, name);

        let mut ctx = Context::empty()
            .with_data_stack(vec![INT(1), INT(2)])
            .with_words(vec![word])
            .with_mode(Mode::Compile);

        let x: Vec<&str> = vec![];
        if let Ok(_x) = interpret(&mut ctx, "eager_plus", &mut x.into_iter()) {
            assert_eq!(ctx.data_stack_len(), 1);
            assert_eq!(ctx.pop().unwrap(), INT(3))
        }
    }

    #[test]
    fn just_compile() {
        let code: NativeCode = rc::Rc::new(|x, _| {
            if let (INT(x1), INT(x2)) = (x.pop()?, x.pop()?) {
                x.push(INT(x1 + x2));
                context::ok(1)
            } else {
                Err(Error::Terminated)
            }
        });
        let immediate = false;
        let primitive = false;
        let name = "eager_plus";
        let word = Word::from(code, immediate, primitive, name);

        let word2 = word.clone();

        let mut ctx = Context::empty()
            .with_data_stack(vec![INT(1), INT(2)])
            .with_words(vec![word])
            .with_mode(Mode::Compile)
            .with_latest(word2);

        println!("{:?}", ctx);

        let x: Vec<&str> = vec![];
        if let Ok(_) = interpret(&mut ctx, "eager_plus", &mut x.into_iter()) {
            // left untouched
            assert_eq!(ctx.data_stack_len(), 2);
            assert_eq!(ctx.unsafe_latest().name, String::from("eager_plus"));
        }
    }
}
