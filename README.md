# brevis

A tiny Forth-like language in Rust for fun (zero dependencies).

It's very much WIP, only environment is boring REPL without history.

If time permits we might build a usable standard library (including a few data processing utilities) and extend the language with an interactive environment.

# TODO -- Add docs and examples

