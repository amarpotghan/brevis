#![allow(dead_code)]
use crate::context::*;

/*
All primitive functions are of the form: ExceptT Error (State (Context, Stream)) Jump.
We do preserve partial state and clear explicitly in outer interpreter
But in reality we leverage mutable ref for both Context and Stream, so they really are of form: (&mut Context, &mut Stream) -> Result<Jump, Err>
Most of these functions could have been defined directly in the language but to avoid the build step, no words are defined in the the brevis language yet(!)
*/

// TODO: Should introduce our own Bool type.
pub const TRUE: i32 = 1;
pub const FALSE: i32 = 0;
pub const VTRUE: Value = Value::INT(TRUE);
pub const VFALSE: Value = Value::INT(FALSE);

pub fn truee() -> NativeCode {
    primitive(move |ctx, _next| {
        ctx.push(VTRUE);
        ok(1)
    })
}

pub fn falsee() -> NativeCode {
    primitive(move |ctx, _next| {
        ctx.push(VFALSE);
        ok(1)
    })
}

pub fn swap() -> NativeCode {
    primitive(|ctx, _next| {
        ctx.check_size(2)?;
        let v1 = ctx.pop()?;
        let v2 = ctx.pop()?;
        ctx.push(v1);
        ctx.push(v2);
        ok(1)
    })
}

// ( n — n n )
pub fn dup() -> NativeCode {
    primitive(|ctx, _next| {
        ctx.check_size(1)?;
        let n = ctx.peek()?;
        ctx.push(n);
        ok(1)
    })
}

// ( n1 n2 — n1 n2 n1 )
pub fn over() -> NativeCode {
    primitive(|ctx, _next| {
        ctx.check_size(2)?;
        let n2 = ctx.pop()?;
        let n1 = ctx.peek()?;
        ctx.push(n2);
        ctx.push(n1);
        ok(1)
    })
}

// ( n1 n2 n3 — n2 n3 n1 )
pub fn rot() -> NativeCode {
    primitive(|ctx, _next| {
        ctx.check_size(3)?;
        let n3 = ctx.pop()?;
        let n2 = ctx.pop()?;
        let n1 = ctx.pop()?;
        ctx.push(n2);
        ctx.push(n3);
        ctx.push(n1);
        ok(1)
    })
}

pub fn to_r() -> NativeCode {
    primitive(|ctx, _next| {
        ctx.check_size(1)?;
        let p = ctx.pop()?;
        ctx.push_return(p);
        ok(1)
    })
}

pub fn from_r() -> NativeCode {
    primitive(|ctx, _next| {
        ctx.check_size_return(1)?;
        let p = ctx.pop_return()?;
        ctx.push(p);
        ok(1)
    })
}

pub fn r_at() -> NativeCode {
    primitive(|ctx, _next| {
        ctx.check_size_return(1)?;
        let p = ctx.peek_return()?;
        ctx.push(p);
        ok(1)
    })
}

pub fn quote() -> NativeCode {
    primitive(|ctx, next| {
        match next.next().and_then(|x| ctx.find(x).map(|_| x)) {
            Some(w) => {
                // taking a shortcut, just pushing name on the stack.
                // execute then blindly expects this word to present.
                ctx.push(Value::WORD(String::from(w)));
                ok(1)
            }
            None => Err(Error::Unexpected(String::from("Quoted word not found"))),
        }
    })
}

pub fn execute() -> NativeCode {
    primitive(|ctx, next| {
        ctx.check_size(1)?;
        match ctx.pop()? {
            Value::WORD(s) => {
                let code = ctx.find(&s).unwrap().code.clone();
                code(ctx, next)
            }

            c => Err(Error::TypeMismatch(
                "execute".to_string(),
                "Word".to_string(),
                c.type_tag().to_string(),
            )),
        }
    })
}

pub fn show_stack() -> NativeCode {
    primitive(|ctx, _next| {
        println!("{}", ctx.show_stack());
        ok(1)
    })
}

// Inner interpreter
pub fn do_colon(name: String) -> NativeCode {
    primitive(move |ctx, next| {
        let word = ctx.find(&name).unwrap().clone();
        let actuals = word.next_words;
        let mut index: usize = 0;

        while index < actuals.len() {
            let w = &actuals[index];
            let r = (w.code)(ctx, next);
            match r {
                Ok(Jump::Forward(next)) => index = index + next,
                Ok(Jump::Backword(next)) => index = index - next,
                Err(err) => return Err(err),
            }
        }
        ok(1)
    })
}

pub fn colon() -> NativeCode {
    primitive(|ctx, next| {
        ctx.switch_to_compile();
        match next.next() {
            Some(name) => {
                ctx.add_latest(Some(Word::user(name, do_colon(String::from(name)), false)));
                ok(1)
            }
            None => Err(Error::Unexpected("Incomplete input".to_string())),
        }
    })
}

pub fn semi() -> NativeCode {
    primitive(|ctx, _| {
        ctx.switch_to_interpret();
        ctx.finish_definition()?;
        ctx.add_latest(None);
        ok(1)
    })
}

fn define_numeric_single(def: impl Fn(Value) -> Result<Value, Error> + 'static) -> NativeCode {
    primitive(move |ctx, _next| {
        ctx.check_size(1)?;
        let v = ctx.pop()?;
        ctx.push(def(v)?);
        ok(1)
    })
}

fn define_numeric_double(
    def: impl Fn(Value, Value) -> Result<Value, Error> + 'static,
) -> NativeCode {
    primitive(move |ctx, _next| {
        ctx.check_size(2)?;
        let v2 = ctx.pop()?;
        let v1 = ctx.pop()?;
        let result = def(v1, v2)?;
        ctx.push(result);
        ok(1)
    })
}
pub fn drop() -> NativeCode {
    primitive(|ctx, _| {
        ctx.check_size(1)?;
        ctx.pop()?;
        ok(1)
    })
}

pub fn dot() -> NativeCode {
    primitive(|ctx, _| {
        ctx.check_size(1)?;
        let v = ctx.pop()?;

        let display = match v {
            Value::STRING(s) => s,
            Value::INT(i) => i.to_string(),
            Value::ADDRESS(_) => String::from("Jump of a word"),
            Value::WORD(_) => String::from("Word"),
            Value::Undefined => String::from("undefined"),
        };

        println!("{}", display);
        ok(1)
    })
}

pub fn comment() -> NativeCode {
    primitive(|_, next| {
        let new = next.take_while(|c| *c != ")");
        let _discard2 = new.count();
        ok(1)
    })
}

pub fn pif() -> NativeCode {
    primitive(|ctx, _| {
        ctx.compile_only()?;
        let code: NativeCode = primitive(|ctx, _next| {
            ctx.check_size(1)?;
            match ctx.pop()? {
                Value::INT(i) => {
                    if i == TRUE {
                        ok(1)
                    } else {
                        Err(Error::IfWithNoElseThen)
                    }
                }
                v => Err(Error::TypeMismatch(
                    "if".to_string(),
                    "INT".to_string(),
                    v.type_tag().to_string(),
                )),
            }
        });

        let word = Word::primitive("IfToken", code);

        ctx.push_variable(AddressTag::If(ctx.current_instruction_address()));
        ctx.add_to_latest(word);
        ok(1)
    })
}

pub fn pelse() -> NativeCode {
    primitive(|ctx, _| {
        ctx.compile_only()?;
        if let Value::ADDRESS(AddressTag::If(address)) = ctx.peek_return()? {
            // place to start execution of else part
            let else_address = ctx.current_instruction_address().absolute();
            let elsee = AddressTag::Else(Jump::Forward(else_address));
            let code: NativeCode = primitive(|_, _| Err(Error::IfWithNoElseThen));
            let word = Word::primitive("ElseToken", code);
            ctx.add_to_latest(word);
            ctx.push_variable(elsee);
            ctx.update_native_code_if(
                address,
                Jump::Forward(else_address - address.absolute() + 1),
            )
        } else {
            Err(Error::IfWithNoElseThen)
        }
    })
}

pub fn then() -> NativeCode {
    primitive(|ctx, _| {
        ctx.compile_only()?;
        ctx.check_size_return(1)?;
        let then_address = ctx.current_instruction_address().absolute();
        match ctx.pop_return()? {
            Value::ADDRESS(AddressTag::Else(Jump::Forward(address))) => {
                let _discard_if_token = ctx.pop_return()?;
                ctx.update_native_code_if(
                    Jump::Forward(address),
                    Jump::Forward(then_address - address),
                )
            }
            Value::ADDRESS(AddressTag::If(address)) => {
                ctx.update_native_code_if(address, Jump::Forward(then_address - address.absolute()))
            }
            _ => Err(Error::IfWithNoElseThen),
        }
    })
}

pub fn immediate() -> NativeCode {
    primitive(|ctx, _| {
        ctx.mark_immediate();
        ok(1)
    })
}

pub fn doo() -> NativeCode {
    primitive(|ctx, _| {
        ctx.compile_only()?;
        let do_native_runtime: NativeCode = primitive(|ctx, _| {
            ctx.check_size(2)?;
            let index = ctx.pop()?;
            let limit = ctx.pop()?;
            match (index, limit) {
                (i @ Value::INT(_), j @ Value::INT(_)) => {
                    ctx.push_return(j);
                    ctx.push_return(i);
                    ok(1)
                }
                (v, _v1) => Err(Error::TypeMismatch(
                    "do".to_string(),
                    String::from("INT"),
                    v.type_tag().to_string(),
                )),
            }
        });

        let word = Word::primitive("DoToken", do_native_runtime);
        let address = ctx.current_instruction_address();
        ctx.add_to_latest(word);
        ctx.push_return(Value::ADDRESS(AddressTag::Do(address)));
        ok(1)
    })
}

pub fn loop_helper(name: &'static str, code: impl Fn(usize) -> NativeCode + 'static) -> NativeCode {
    primitive(move |ctx, _| {
        ctx.compile_only()?;
        ctx.check_size_return(1)?;
        match ctx.pop_return()? {
            Value::ADDRESS(AddressTag::Do(address)) => {
                let current_address = ctx.current_instruction_address();
                let jump_address = current_address.absolute() - address.absolute();
                let w = Word::primitive(name, code(jump_address - 1));
                ctx.add_to_latest(w);
                ok(1)
            }
            v => Err(Error::TypeMismatch(
                String::from("loop/+loop"),
                String::from("ADDRESS"),
                v.type_tag().to_string(),
            )),
        }
    })
}
pub fn loopy() -> NativeCode {
    loop_helper("LoopToken", |jump_address: usize| {
        primitive(move |ctx: &mut Context, _| {
            let index = ctx.pop_return()?;
            let limit = ctx.peek_return()?;
            match (index, limit) {
                (Value::INT(i), Value::INT(j)) => {
                    let next = i + 1;
                    if next >= j {
                        ctx.pop_return()?;
                        ok(1)
                    } else {
                        ctx.push_return(Value::INT(i + 1));
                        Ok(Jump::Backword(jump_address))
                    }
                }

                (v, _) => Err(Error::TypeMismatch(
                    String::from("loop"),
                    String::from("INT"),
                    v.type_tag().to_string(),
                )),
            }
        })
    })
}

fn code_from(n: String) -> NativeCode {
    primitive(move |ctx1, _| {
        ctx1.push_return(Value::WORD(n.clone()));
        ok(1)
    })
}

fn variable_helper(capacity: usize) -> NativeCode {
    primitive(move |ctx, next| {
        let maybe_name = next.next();
        match maybe_name {
            Some(n) => {
                let word = code_from(String::from(n));
                ctx.allocate(Word::primitive(n, word), capacity);
                ok(1)
            }
            None => ok(1),
        }
    })
}

// variable is just a special case of an array
pub fn variable() -> NativeCode {
    variable_helper(1)
}

pub fn array() -> NativeCode {
    primitive(|ctx, next| match ctx.pop()? {
        Value::INT(capacity) => variable_helper(capacity as usize)(ctx, next),
        v => Err(Error::TypeMismatch(
            "array".to_string(),
            "INT".to_string(),
            v.type_tag().to_string(),
        )),
    })
}

pub fn store() -> NativeCode {
    primitive(|ctx, _next| {
        ctx.check_size_return(1)?;
        match ctx.pop_return()? {
            Value::WORD(word) => {
                ctx.store(word)?;
                ok(1)
            }
            v => Err(Error::TypeMismatch(
                "!".to_string(),
                "WORD".to_string(),
                v.type_tag().to_string(),
            )),
        }
    })
}

pub fn access() -> NativeCode {
    primitive(|ctx, _next| {
        match ctx.pop_return()? {
            Value::WORD(word) => {
                let data = ctx.access(&word)?;
                ctx.push(data);
                ok(1)
            }
            v => Err(Error::TypeMismatch(
                "@".to_string(),
                "WORD".to_string(),
                v.type_tag().to_string(),
            )),
        }
    })
}

pub fn cr() -> NativeCode {
    primitive(|_, _| {
        println!("");
        ok(1)
    })
}

pub fn plus_loop() -> NativeCode {
    loop_helper("PlusLoopToken", |jump_address: usize| {
        primitive(move |ctx: &mut Context, _| {
            let index = ctx.pop_return()?;
            let limit = ctx.peek_return()?;
            let inc_by = ctx.pop()?;
            match (index, limit, inc_by) {
                (Value::INT(i), Value::INT(limit), Value::INT(inc_by)) => {
                    let next = i + inc_by;
                    let stop = if inc_by >= 0 {
                        next >= limit
                    } else {
                        next <= limit
                    };
                    if stop {
                        ctx.pop_return()?;
                        ok(1)
                    } else {
                        ctx.push_return(Value::INT(next));
                        Ok(Jump::Backword(jump_address))
                    }
                }
                (v, _, _) => Err(Error::TypeMismatch(
                    String::from("+loop"),
                    String::from("INT"),
                    v.type_tag().to_string(),
                )),
            }
        })
    })
}

pub fn leave() -> NativeCode {
    primitive(|ctx, _| {
        ctx.compile_only()?;
        let code: NativeCode = primitive(|ctx, _| {
            ctx.check_size_return(2)?;
            let _index = ctx.pop_return()?;
            let limit = ctx.peek_return()?;
            ctx.push_return(limit);
            ok(1)
        });

        let w = Word::primitive("LeaveToken", code);
        ctx.add_to_latest(w);
        ok(1)
    })
}

pub fn i() -> NativeCode {
    primitive(|ctx, _| {
        ctx.check_size_return(1)?;
        let index = ctx.peek_return()?;
        ctx.push(index);
        ok(1)
    })
}

pub fn j() -> NativeCode {
    primitive(|ctx, _| {
        ctx.check_size_return(3)?;
        let first_index = ctx.pop_return()?;
        let first_limit = ctx.pop_return()?;
        let second_index = ctx.peek_return()?;
        ctx.push(second_index);
        ctx.push_return(first_limit);
        ctx.push_return(first_index);
        ok(1)
    })
}

pub fn forget() -> NativeCode {
    primitive(|ctx, next| {
        if let Some(wn) = next.next() {
            ctx.remove_user_defined(wn);
            ok(1)
        } else {
            Err(Error::Unexpected(String::from("Insufficient input")))
        }
    })
}

pub fn begin() -> NativeCode {
    primitive(|ctx, _| {
        ctx.compile_only()?;
        let begin_address = ctx.current_instruction_address();
        ctx.push_return(Value::ADDRESS(AddressTag::Begin(begin_address)));
        ok(1)
    })
}
//  1  2  3  4  5
// : x a run x  y end ;

pub fn until() -> NativeCode {
    primitive(|ctx, _| {
        ctx.compile_only()?;
        ctx.check_size_return(1)?;
        match ctx.pop_return()? {
            Value::ADDRESS(AddressTag::Begin(addr)) => {
                let current_addr = ctx.current_instruction_address();
                let code = primitive(move |ctx, _| {
                    ctx.check_size(1)?;
                    let v = ctx.pop()?;
                    if v == Value::INT(FALSE) {
                        let x = current_addr.absolute() - addr.absolute();
                        Ok(Jump::Backword(x))
                    } else {
                        ok(1)
                    }
                });

                ctx.add_to_latest(Word::primitive("UntilToken", code));
                ok(1)
            }

            _ => Err(Error::Unexpected(String::from(
                "End should always have corresponding begin",
            ))),
        }
    })
}

pub fn constant() -> NativeCode {
    primitive(|ctx, next| {
        if let Some(wn) = next.next() {
            ctx.check_size(1)?;
            match ctx.pop()?.as_lit_word() {
                Some(val) => {
                    let code: NativeCode = primitive(|_, _| ok(1));
                    let mut word = Word::user(wn, code, false);
                    word.next_words = vec![val];
                    ctx.add_word(word);
                    ok(1)
                }
                None => Err(Error::Unexpected(String::from(
                    "Only supported literals are string and int",
                ))),
            }
        } else {
            Err(Error::Unexpected(String::from("Insufficient input")))
        }
    })
}

pub fn dictionary() -> NativeCode {
    primitive(|ctx, _| {
        let all = ctx.all_defined();
        all.iter().for_each(|x| println!("{}", x));
        ok(1)
    })
}

pub fn bye() -> NativeCode {
    primitive(|_, _| Err(Error::Terminated))
}

pub fn current_node() -> NativeCode {
    primitive(|ctx, _| {
        println!("{}", ctx.current_mode());
        ok(1)
    })
}

pub fn inspect_word() -> NativeCode {
    primitive(|ctx, _| {
        if let Value::STRING(word_str) = ctx.pop()? {
            let word = ctx.find(&word_str);
            println!("{:?}", word);
            ok(1)
        } else {
            Err(Error::Unexpected(String::from("Not a string on stack")))
        }
    })
}

pub fn new_context() -> Context {
    let words = vec![
        Word::primitive_immediate("if", pif()),
        Word::primitive_immediate("then", then()),
        Word::primitive_immediate("else", pelse()),
        Word::primitive_immediate("do", doo()),
        Word::primitive_immediate(";", semi()),
        Word::primitive_immediate("(", comment()),
        Word::primitive_immediate("loop", loopy()),
        Word::primitive_immediate("+loop", plus_loop()),
        Word::primitive_immediate("begin", begin()),
        Word::primitive_immediate("until", until()),
        Word::primitive_immediate("leave", leave()),
        Word::primitive("dup", dup()),
        Word::primitive("true", truee()),
        Word::primitive("false", falsee()),
        Word::primitive("1+", define_numeric_single(|i| i + Value::INT(1))),
        Word::primitive("2+", define_numeric_single(|i| i + Value::INT(2))),
        Word::primitive("swap", swap()),
        Word::primitive("over", over()),
        Word::primitive("rot", rot()),
        Word::primitive("+", define_numeric_double(|i, j| i + j)),
        Word::primitive("*", define_numeric_double(|i, j| i * j)),
        Word::primitive("2*", define_numeric_single(|i| Value::INT(2) * i)),
        Word::primitive("1-", define_numeric_single(|i| i - Value::INT(1))),
        Word::primitive("2-", define_numeric_single(|i| i - Value::INT(2))),
        Word::primitive(
            "=",
            define_numeric_double(|i, j| if i == j { Ok(VTRUE) } else { Ok(VFALSE) }),
        ),
        Word::primitive(
            "=0",
            define_numeric_single(|i| if i == VFALSE { Ok(VTRUE) } else { Ok(VFALSE) }),
        ),
        Word::primitive("-", define_numeric_double(|i, j| i - j)),
        Word::primitive("neg", define_numeric_single(|i| VFALSE - i)),
        Word::primitive(
            "<0",
            define_numeric_single(|i| if i < VFALSE { Ok(VTRUE) } else { Ok(VFALSE) }),
        ),
        Word::primitive(
            "<",
            define_numeric_double(|i, j| if i < j { Ok(VTRUE) } else { Ok(VFALSE) }),
        ),
        Word::primitive(
            ">",
            define_numeric_double(|i, j| if i > j { Ok(VTRUE) } else { Ok(VFALSE) }),
        ),
        Word::primitive("abs", define_numeric_single(|i| Value::abs(i))),
        Word::primitive("mod", define_numeric_double(|i, j| j % i)),
        Word::primitive("immediate", immediate()),
        Word::primitive(":", colon()),
        Word::primitive("or", define_numeric_double(|i, j| i | j)),
        Word::primitive("and", define_numeric_double(|i, j| i & j)),
        Word::primitive("xor", define_numeric_double(|i, j| i ^ j)),
        Word::primitive("min", define_numeric_double(|i, j| Ok(std::cmp::min(i, j)))),
        Word::primitive("max", define_numeric_double(|i, j| Ok(std::cmp::max(i, j)))),
        Word::primitive("'", quote()),
        Word::primitive("execute", execute()),
        Word::primitive(">r", to_r()),
        Word::primitive("r>", from_r()),
        Word::primitive("r@", r_at()),
        Word::primitive("i", i()),
        Word::primitive("j", j()),
        Word::primitive("forget", forget()),
        Word::primitive("drop", drop()),
        Word::primitive(".", dot()),
        Word::primitive("show", dictionary()),
        Word::primitive("cr", cr()),
        Word::primitive("variable", variable()),
        Word::primitive("constant", constant()),
        Word::primitive("!", store()),
        Word::primitive("inspect_mode", current_node()),
        Word::primitive("inspect_word", inspect_word()),
        Word::primitive("bye", bye()),
        Word::primitive("array", array()),
        Word::primitive("@", access()),
        Word::primitive("show_stack", show_stack()),
    ];

    Context::empty().with_words(words)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn plus() -> NativeCode {
        define_numeric_double(|i, j| i + j)
    }
    fn one_plus() -> NativeCode {
        define_numeric_single(|i| i + Value::INT(1))
    }

    #[test]
    fn do_colon_test1() {
        let add = Word::from(plus(), false, true, "plus");
        let latest_word = Word::from(one_plus(), false, false, "Something").with_next_words(vec![
            lit_word(Value::INT(1)),
            lit_word(Value::INT(2)),
            add,
        ]);
        let f: NativeCode = do_colon(String::from("Something"));
        let mut ctx = Context::empty().with_words(vec![latest_word]);

        let x: Vec<&str> = vec![];
        let _x = (f)(&mut ctx, &mut x.into_iter());
        assert_eq!(ctx.pop().unwrap(), Value::INT(3))
    }

    #[test]
    fn do_colon_test() {
        let dy: NativeCode = primitive(|ctx, _next| {
            ctx.push(Value::INT(2));
            ctx.push(Value::INT(2));
            // jump 3 instructions
            ok(3)
        });

        let add = Word::from(plus(), false, true, "plus");
        let dyw = Word::from(dy, false, true, "dy");
        let latest_word = Word::from(one_plus(), false, false, "Something").with_next_words(vec![
            dyw,
            lit_word(Value::INT(1)),
            lit_word(Value::INT(2)),
            add,
        ]);
        let f: NativeCode = do_colon(String::from("Something"));
        let mut ctx = Context::empty().with_words(vec![latest_word]);
        let x: Vec<&str> = vec![];
        let _x = (f)(&mut ctx, &mut x.into_iter());
        assert_eq!(ctx.pop().unwrap(), Value::INT(4));
    }

    #[test]
    fn comment_test() {
        let l = "( -- )";
        let mut c = Context::empty();
        let mut i = l.split_whitespace();
        if let Ok(Jump::Forward(1)) = comment()(&mut c, &mut i) {};
        assert_eq!(i.next(), None)
    }

    #[test]
    fn comment_test_2() {
        let l = "( -- ) 1";
        let mut c = Context::empty();
        let mut i = l.split_whitespace();
        if let Ok(Jump::Forward(1)) = comment()(&mut c, &mut i) {};
        assert_eq!(i.next(), Some("1"))
    }

    #[test]
    fn comment_test_3() {
        let l = "( -- ) 1 2 3";
        let mut c = Context::empty();
        let mut i = l.split_whitespace();
        if let Ok(Jump::Forward(1)) = comment()(&mut c, &mut i) {};
        assert_eq!(i.next(), Some("1"));
        assert_eq!(i.next(), Some("2"));
        assert_eq!(i.next(), Some("3"));
    }
}

/**
Unused, because incomplete
 */
type Go = Result<Jump, Error>;

macro_rules! native {
    ($name:ident, $body:expr) => {
        struct $name;
        impl<'a> Native<'a> for $name {
            fn execute(context: &mut Context, next: &mut impl Iterator<Item = &'a str>) -> Go {
                $body(context, next)
            }
        }
    };
}

native!(True, |ctx: &mut Context, _next| {
    ctx.push(VTRUE);
    ok(1)
});

native!(False, |ctx: &mut Context, _next| {
    ctx.push(VFALSE);
    ok(1)
});

native!(Swap, |ctx: &mut Context, _next| {
    ctx.check_size(2)?;
    let v1 = ctx.pop()?;
    let v2 = ctx.pop()?;
    ctx.push(v1);
    ctx.push(v2);
    ok(1)
});

native!(Dup, |ctx: &mut Context, _next| {
    ctx.check_size(1)?;
    let n = ctx.peek()?;
    ctx.push(n);
    ok(1)
});

native!(Over, |ctx: &mut Context, _next| {
    ctx.check_size(2)?;
    let n2 = ctx.pop()?;
    let n1 = ctx.peek()?;
    ctx.push(n2);
    ctx.push(n1);
    ok(1)
});

native!(Rot, |ctx: &mut Context, _next| {
    ctx.check_size(3)?;
    let n3 = ctx.pop()?;
    let n2 = ctx.pop()?;
    let n1 = ctx.pop()?;
    ctx.push(n2);
    ctx.push(n3);
    ctx.push(n1);
    ok(1)
});

native!(ToR, |ctx: &mut Context, _next| {
    ctx.check_size(1)?;
    let p = ctx.pop()?;
    ctx.push_return(p);
    ok(1)
});

native!(FromR, |ctx: &mut Context, _next| {
    ctx.check_size_return(1)?;
    let p = ctx.pop_return()?;
    ctx.push(p);
    ok(1)
});

native!(AtR, |ctx: &mut Context, _next| {
    ctx.check_size_return(1)?;
    let p = ctx.peek_return()?;
    ctx.push(p);
    ok(1)
});

native!(
    Quote,
    |ctx: &mut Context, next: &mut dyn Iterator<Item = &str>| {
        match next.next().and_then(|x| ctx.find(x).map(|_| x)) {
            Some(w) => {
                // taking a shortcut, just pushing name on the stack.
                // execute then blindly expects this word to present.
                ctx.push(Value::WORD(String::from(w)));
                ok(1)
            }
            None => Err(Error::Unexpected(String::from("Quoted word not found"))),
        }
    }
);

native!(
    Execute,
    |ctx: &mut Context, next: &mut dyn Iterator<Item = &str>| {
        ctx.check_size(1)?;
        match ctx.pop()? {
            Value::WORD(s) => {
                let code = ctx.find(&s).unwrap().code.clone();
                code(ctx, next)
            }

            c => Err(Error::TypeMismatch(
                "execute".to_string(),
                "Word".to_string(),
                c.type_tag().to_string(),
            )),
        }
    }
);
