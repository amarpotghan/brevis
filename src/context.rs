use std::collections::HashMap;
use std::fmt;
use std::ops::{Add, BitAnd, BitOr, BitXor, Mul, Rem, Sub};
use std::rc;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Copy)]
pub enum Jump {
    Forward(usize),
    Backword(usize),
}

impl Jump {
    pub fn absolute(&self) -> usize {
        match self {
            Jump::Forward(address) => *address,
            Jump::Backword(address) => *address,
        }
    }
}

pub fn ok<E>(i: usize) -> Result<Jump, E> {
    Ok(Jump::Forward(i))
}
pub type Token = dyn Fn(&mut Context, &mut dyn Iterator<Item = &str>) -> Result<Jump, Error>;
pub type NativeCode = rc::Rc<Token>;

pub fn primitive(
    token: impl Fn(&mut Context, &mut dyn Iterator<Item = &str>) -> Result<Jump, Error> + 'static,
) -> NativeCode {
    rc::Rc::new(move |ctx, next| token(ctx, next))
}

#[allow(dead_code)]
#[derive(Debug, Clone, PartialEq)]
pub enum Error {
    Terminated,
    UnxpectedWord(String, String),
    Unexpected(String),
    // TODO: add given type too
    TypeMismatch(String, String, String),
    StackUnderflow(String, usize, usize),
    IfWithNoElseThen,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Terminated => write!(f, "Bye."),
            Error::UnxpectedWord(word, parse_error) => {
                write!(f, "I don't understand the word {}. \
                           A word can either be already defined word or an integer literal (e.g. 10, 99 etc.) \
                           or a string literal (e.g. \"Stack\"). \n\n \
                           Internal parsing error: {}", word, parse_error)
            }
            Error::TypeMismatch(context, expected, given) => write!(f, "Type mismatch expected: {} but given: {} while interpreting {}", expected, given, context),
            Error::StackUnderflow(stack, expected, actual) =>
                write!(f, "{} underflow. You are assuming there are at least {} values on stack \
                           but in reality there are only {} available", stack, expected, actual),
            Error::IfWithNoElseThen =>
                write!(f, "You are trying to use if without following then or else. \
                           \n\n Consider adding then after your 'true clause' like `: gt_5? > 5 if \"GreaterThan5\" then ;`"),
            Error::Unexpected(st) => write!(f, "I encountered an unexpected error while interpreting: \
                                                {}. \n\n", st),
        }
    }
}

impl Error {
    pub fn unexpected_word(word: &str, err: &str) -> Error {
        let formatted_word = format!("{}?", word);
        Error::UnxpectedWord(formatted_word, err.to_string())
    }
}

pub struct Word {
    pub code: NativeCode,
    pub immediate: bool,
    primitive: bool,
    pub name: String,
    // used to store compiled words
    pub next_words: Vec<Word>,
}

impl Word {
    pub fn primitive(name: &str, code: NativeCode) -> Word {
        Word::from(code, false, true, name)
    }

    pub fn user(name: &str, code: NativeCode, immediate: bool) -> Word {
        Word::from(code, immediate, false, name)
    }

    pub fn primitive_immediate(name: &str, code: NativeCode) -> Word {
        Word::from(code, true, true, name)
    }
    pub fn from(code: NativeCode, immediate: bool, primitive: bool, name: &str) -> Word {
        Word {
            code,
            immediate,
            primitive,
            name: name.to_string(),
            next_words: vec![],
        }
    }

    pub fn mark_immediate(&mut self) {
        self.immediate = true;
    }

    #[allow(dead_code)]
    pub fn with_next_words(self, next_words: Vec<Word>) -> Self {
        Self { next_words, ..self }
    }
}

impl Clone for Word {
    fn clone(&self) -> Self {
        Word {
            code: self.code.clone(),
            immediate: self.immediate,
            primitive: self.primitive,
            name: self.name.clone(),
            next_words: self.next_words.clone(),
        }
    }
}

impl fmt::Display for Word {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

impl fmt::Debug for Word {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "(name: {}, immediate: {}, primitive: {}, next_words: {:?})",
            self.name, self.immediate, self.primitive, self.next_words
        )
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum AddressTag {
    If(Jump),
    Else(Jump),
    Do(Jump),
    Begin(Jump),
}

#[allow(dead_code)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Value {
    INT(i32),
    STRING(String),
    ADDRESS(AddressTag),
    WORD(String),
    Undefined,
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let val = match self {
            Value::INT(i) => i.to_string(),
            Value::STRING(s) => s.to_string(),
            Value::ADDRESS(_) => String::from("AddressTag"),
            Value::WORD(_) => String::from("Word"),
            Value::Undefined => String::from("undefined"),
        };
        write!(f, "{}", val)
    }
}

impl Value {
    pub fn abs(v: Value) -> Result<Value, Error> {
        match v {
            Value::INT(i) => Ok(Value::INT(i.abs())),
            p => Err(Error::TypeMismatch(
                "abs".to_string(),
                "INT".to_string(),
                p.type_tag().to_string(),
            )),
        }
    }

    pub fn type_tag(&self) -> &str {
        match self {
            Value::INT(_) => "Int",
            Value::STRING(_) => "String",
            Value::ADDRESS(_) => "AddressTag",
            Value::WORD(_) => "Word",
            Value::Undefined => "Undefined",
        }
    }

    pub fn as_lit_word(&self) -> Option<Word> {
        match self {
            v @ Value::INT(_) => Some(lit_word(v.clone())),
            v @ Value::STRING(_) => Some(lit_word(v.clone())),
            _ => None,
        }
    }
}

pub fn lit_word(v: Value) -> Word {
    let code = primitive(move |ctx, _next| {
        ctx.push(v.clone());
        ok(1)
    });
    Word::user("", code, false)
}

macro_rules! impl_for_value {
    ($typeclass: ident, $method: ident, $op: tt) => {
        impl $typeclass for Value {
            type Output = Result<Self, Error>;
            fn $method(self, other: Self) -> Self::Output {
                match (self, other) {
                    (Value::INT(i), Value::INT(j)) => Ok(Value::INT(i $op j)),
                    (o1, _o2) => Err(Error::TypeMismatch(stringify!{$method}.to_string(), stringify!{$pattern}.to_string(), o1.type_tag().to_string()))
                }
            }
        }
    };
}

impl_for_value!(Add, add, +);
impl_for_value!(Mul, mul, *);
impl_for_value!(Sub, sub, -);
impl_for_value!(Rem, rem, %);
impl_for_value!(BitAnd, bitand, &);
impl_for_value!(BitOr, bitor, |);
impl_for_value!(BitXor, bitxor, ^);

#[derive(Debug, Clone, PartialEq)]
pub enum Mode {
    Compile,
    Interpret,
}

impl fmt::Display for Mode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let display = match self {
            Mode::Compile => "Compile",
            Mode::Interpret => "Interpret",
        };
        write!(f, "{}", display)
    }
}

#[derive(Debug)]
pub struct Context {
    data_stack: Vec<Value>,
    return_stack: Vec<Value>,
    data_space: HashMap<String, Vec<Value>>,
    words: Vec<Word>,
    mode: Mode,
    latest: Option<Word>,
}

impl Context {
    pub fn empty() -> Context {
        Context {
            data_stack: vec![],
            return_stack: vec![],
            words: vec![],
            data_space: HashMap::new(),
            mode: Mode::Interpret,
            latest: None,
        }
    }

    #[allow(dead_code)]
    pub fn with_data_stack(self, data_stack: Vec<Value>) -> Self {
        Context { data_stack, ..self }
    }

    pub fn with_words(self, words: Vec<Word>) -> Self {
        Context { words, ..self }
    }

    #[allow(dead_code)]
    pub fn with_mode(self, mode: Mode) -> Self {
        Context { mode, ..self }
    }

    #[allow(dead_code)]
    pub fn with_latest(self, word: Word) -> Self {
        Context {
            latest: Some(word),
            ..self
        }
    }

    fn pop_or<A: Clone>(v: &mut Vec<A>, stack: &str) -> Result<A, Error> {
        v.pop()
            .ok_or(Error::StackUnderflow(format!("{}", stack), 1, 0))
            .map(|x| x.clone())
    }
    pub fn pop(&mut self) -> Result<Value, Error> {
        Context::pop_or(&mut self.data_stack, "data stack")
    }

    pub fn pop_return(&mut self) -> Result<Value, Error> {
        Context::pop_or(&mut self.return_stack, "return stack")
    }

    pub fn allocate(&mut self, word: Word, capacity: usize) {
        let preallocated = vec![Value::Undefined; capacity];
        self.data_space.insert(word.name.clone(), preallocated);
        self.words.push(word)
    }

    pub fn store(&mut self, word: String) -> Result<(), Error> {
        if self.array_variable(&word)? {
            match self.pop()? {
                Value::INT(at) => self.store_array(&word, at as usize),
                v => Err(Error::TypeMismatch(
                    "!".to_string(),
                    "INT".to_string(),
                    v.type_tag().to_string(),
                )),
            }
        } else {
            self.store_variable(word)
        }
    }

    pub fn access(&mut self, word: &str) -> Result<Value, Error> {
        if self.array_variable(&word)? {
            match self.pop()? {
                Value::INT(at) => self
                    .data_space
                    .get(word)
                    .and_then(|v| v.get(at as usize))
                    .map(|x| x.clone())
                    .ok_or(Error::Unexpected(format!(
                        "data for array {} at {} does not exist",
                        word, at
                    ))),
                v => Err(Error::TypeMismatch(
                    String::from("access"),
                    String::from("INT"),
                    v.type_tag().to_string(),
                )),
            }
        } else {
            self.data_space
                .get(word)
                .and_then(|v| v.first())
                .map(|x| x.clone())
                .ok_or(Error::Unexpected(format!(
                    "data for array {} does not exist",
                    word
                )))
        }
    }
    pub fn array_variable(&self, word: &str) -> Result<bool, Error> {
        self.data_space
            .get(word)
            .map(|x| x.len() > 1)
            .ok_or(Error::UnxpectedWord(
                word.to_string(),
                "variable check".to_string(),
            ))
    }

    pub fn store_array(&mut self, word: &str, at: usize) -> Result<(), Error> {
        let x = self.pop()?;
        let mem = self.data_space.get_mut(word).ok_or(Error::UnxpectedWord(
            word.to_string(),
            "variable check".to_string(),
        ))?;

        let length = mem.len();
        if at < length {
            let _ = std::mem::replace(&mut mem[at], x);
            Ok(())
        } else {
            Err(Error::Unexpected(format!(
                "Wrong array variable offset,
                 it's greater than length of the array.
                 offset: {}, length: {} ",
                at, length
            )))
        }
    }

    pub fn store_variable(&mut self, word: String) -> Result<(), Error> {
        let x = self.pop()?;
        self.data_space.entry(word).and_modify(move |mem| {
            let _ = std::mem::replace(&mut mem[0], x);
        });
        Ok(())
    }

    pub fn current_instruction_address(&self) -> Jump {
        self.latest
            .clone()
            .map_or_else(|| Jump::Forward(0), |w| Jump::Forward(w.next_words.len()))
    }

    pub fn show_stack(&self) -> String {
        self.data_stack
            .iter()
            .fold(String::from(""), |acc, n| format!("{}{} ", acc, n))
            .trim()
            .to_string()
    }

    pub fn all_defined(&self) -> Vec<&str> {
        self.words.iter().map(|x| &x.name[..]).collect()
    }

    fn peek_or<A: Clone>(v: &Vec<A>, stack: &str) -> Result<A, Error> {
        v.last()
            .ok_or(Error::StackUnderflow(format!("{}", stack), 1, 0))
            .map(|x| x.clone())
    }

    pub fn peek_return(&mut self) -> Result<Value, Error> {
        Context::peek_or(&self.return_stack, "return stack")
    }

    pub fn add_word(&mut self, word: Word) {
        self.words.push(word);
    }

    #[allow(dead_code)]
    pub fn data_stack_len(&self) -> usize {
        self.data_stack.len()
    }

    pub fn push(&mut self, v: Value) {
        self.data_stack.push(v);
    }

    pub fn push_return(&mut self, v: Value) {
        self.return_stack.push(v);
    }

    pub fn peek(&self) -> Result<Value, Error> {
        Context::peek_or(&self.data_stack, "data stack")
    }

    pub fn mark_immediate(&mut self) {
        self.words.last_mut().map(|v| v.mark_immediate());
    }

    pub fn find(&self, word: &str) -> Option<&Word> {
        self.words
            .iter()
            .rev()
            .find(|forth_word| word == forth_word.name)
    }

    pub fn remove_user_defined(&mut self, word: &str) {
        self.words
            .iter()
            .rposition(|x| x.primitive && x.name == word)
            .map(|x| self.words.remove(x));
    }

    pub fn add_to_latest(&mut self, word: Word) {
        self.latest.as_mut().map(|x| x.next_words.push(word));
    }

    pub fn push_variable(&mut self, address: AddressTag) {
        self.return_stack.push(Value::ADDRESS(address))
    }

    fn modify_code(&mut self, native: NativeCode, other_address: Jump) -> NativeCode {
        rc::Rc::new(move |ctx, next| {
            match native(ctx, next) {
                // true branch who cares?
                o @ Ok(_) => o,
                // if else/then branch
                Err(Error::IfWithNoElseThen) => Ok(other_address),
                everything_else => everything_else,
            }
        })
    }

    pub fn update_native_code_if(
        &mut self,
        if_address: Jump,
        other_address: Jump,
    ) -> Result<Jump, Error> {
        let mut word = self.unsafe_latest();
        match word.next_words.get_mut(if_address.absolute()) {
            Some(v) => {
                v.code = self.modify_code(v.code.clone(), other_address);
                ok(1)
            }
            None => Err(Error::Unexpected(
                "Wrong if address in else/then instruction".to_string(),
            )),
        }?;

        self.latest = Some(word);
        ok(1)
    }

    pub fn finish_definition(&mut self) -> Result<(), Error> {
        match self.latest.clone() {
            Some(word) => Ok(self.words.push(word)),
            None => Err(Error::Unexpected("Semicolon at wrong place".to_string())),
        }
    }

    pub fn clear(&mut self) {
        self.return_stack.clear();
        self.data_stack.clear();
        self.latest = None;
        self.mode = Mode::Interpret
    }

    #[allow(dead_code)]
    pub fn define_current_word(&mut self) {
        let current_word = self.unsafe_latest();
        self.words.push(current_word);
        self.latest = None;
    }

    pub fn switch_to_interpret(&mut self) {
        self.mode = Mode::Interpret;
    }

    pub fn switch_to_compile(&mut self) {
        self.mode = Mode::Compile;
    }

    pub fn unsafe_latest(&self) -> Word {
        self.latest.clone().unwrap()
    }

    fn check<A>(v: &Vec<A>, expected: usize, stack: &str) -> Result<(), Error> {
        let size = v.len();
        if size >= expected {
            Ok(())
        } else {
            Err(Error::StackUnderflow(format!("{}", stack), expected, size))
        }
    }

    pub fn check_size(&self, expected: usize) -> Result<(), Error> {
        Context::check(&self.data_stack, expected, "data")
    }

    pub fn check_size_return(&self, expected: usize) -> Result<(), Error> {
        Context::check(&self.return_stack, expected, "return")
    }

    pub fn current_mode(&self) -> &Mode {
        &self.mode
    }

    pub fn add_latest(&mut self, opt_word: Option<Word>) {
        self.latest = opt_word;
    }

    pub fn compile_only(&self) -> Result<(), Error> {
        match self.mode {
            Mode::Compile => Ok(()),
            Mode::Interpret => Err(Error::Unexpected(format!(
                "Word only valid in Compile mode"
            ))),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::primitives::*;
    use Value::*;

    #[test]
    fn test_latest() {
        let mut ctx = Context::empty();
        let native: NativeCode = rc::Rc::new(|_, _| Err(Error::IfWithNoElseThen));
        let nn = native.clone();
        let word: Word = Word::user("S", do_colon(String::from("S")), false);
        ctx.latest = Some(word);
        let next_word = Word::user("SS", nn, false);
        ctx.add_to_latest(next_word);

        assert_eq!(ctx.latest.unwrap().next_words.len(), 1);
    }

    #[test]
    fn test_modify_native() {
        let mut context = Context::empty();
        let native: NativeCode = rc::Rc::new(|_, _| Err(Error::IfWithNoElseThen));

        let mut word: Word = Word::user("S", do_colon(String::from("S")), false);
        let to_exec = word.clone();
        word.next_words = vec![Word::user("SS", native, false)];
        context.latest = Some(word);

        if let Ok(Jump::Forward(1)) =
            context.update_native_code_if(Jump::Forward(0), Jump::Forward(1))
        {
            context.define_current_word();
            let r = (to_exec.clone().code)(&mut context, &mut vec![].into_iter());
            assert_eq!(r.unwrap(), Jump::Forward(1));
        }
    }

    #[test]
    fn context() {
        let mut context = Context::empty();
        context.push(STRING(String::from("First")));
        context.push(INT(1));
        let p1 = context.pop().unwrap();
        let p2 = context.pop().unwrap();
        assert_eq!(p2, STRING(String::from("First")));
        assert_eq!(p1, INT(1))
    }
}

// unused
pub trait Native<'a> {
    fn execute(
        context: &mut Context,
        next: &mut impl Iterator<Item = &'a str>,
    ) -> Result<Jump, Error>;
}
